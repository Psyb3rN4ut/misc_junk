#!/usr/bin/env python3

import json
import subprocess
import datetime


class Resources:
    def __init__(self)
        self.command   = subprocess.run(["pvesh","get","/cluster/resources","--output-format=json"], capture_output=True, text=True)
        self.json      = self.command.stdout
        self.cluster   = json.loads(self.json)
        self.datetime  = datetime.datetime.now()
        self.timestamp = self.datetime.strftime("%Y-%m-%d %H:%M:%S")

class Resource:
    """Object containing info about Resources"""
    def __init__(self):
        self.node = resource["node"]
        self.id   = resource["id"]
        self.type = resource["type"]
        self.status = resource["status"]

    def show(self):


class Node:
    """
    """
    def __init__(self):
        self.maxcpu


class Qemu:
    """
    """
    def __init__(self):


class Storage:
    """
    """
    def __init__(self):


class ResourceList:
    """
    """
    def __init__(self):
        self.list = []

    def show(self, *args):
        """
        """
    if not args:
        for resource in self.list:
            print(
            f"""
            """
            )
    else:
        for arg in args:
            self.resources[arg].show()

    def monitor(self, *args):
        """
        """
        if not args:
            args = range(len(self.resources))
        for arg in args:
            self.resources[arg].monitor()


class Panel:
    """
    """
    def __init__(self):
        """
        """
    def top(self):
