#!/usr/bin/env python3

import datetime
import json
import subprocess

resources = "/cluster/resources"
command   = ["pvesh","get","--output-format=","json"]
run       = subprocess.run(
    [command[0],command[1],resources,command[2],command[3],
    capture_output = True,
    text           = True
    ]
    )
cluster_json = run.stdout
cluster = json.loads(cluster_json)
timestamp = self.datetime.strftime("%Y-%m-%d %H:%M:%S")

"""
["cpu","maxdisk","maxmem","mem","name","netin","netout","node","status","template","type"]
"""

keys = ["cpu","disk","diskread","diskwrite","id","maxdisk","maxmem","mem","name","netin","netout","node","status"]

for resource in cluster:
    
    for value in resource:
        if value in keys:
            print(value, resource[value])

