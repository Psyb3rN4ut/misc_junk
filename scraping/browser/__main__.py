#!/usr/bin/env python3

import requests
from bs4 import BeautifulSoup
import urwid

class TextBrowser:
    def __init__(self):
        self.history = []
        self.current_url = ""
        self.text_box = urwid.Edit("Enter URL: ")
        self.listbox = urwid.ListBox(urwid.SimpleFocusListWalker([self.text_box]))

    def fetch_page(self, url):
        try:
            response = requests.get(url)
            response.raise_for_status()
            soup = BeautifulSoup(response.text, 'html.parser')
            return soup
        except requests.RequestException as e:
            print(f"Error fetching page: {e}")
            return None

    def display_page(self, soup):
        if soup:
            # Extract and display the text content of the page
            text_content = soup.get_text()
            self.listbox.body.append(urwid.Text(text_content))
        else:
            self.listbox.body.append(urwid.Text("Failed to display page."))

    def navigate(self, url):
        self.history.append(self.current_url)
        self.current_url = url
        self.listbox.body.clear()
        soup = self.fetch_page(url)
        self.display_page(soup)

    def go_back(self):
        if self.history:
            previous_url = self.history.pop()
            self.navigate(previous_url)
        else:
            self.listbox.body.append(urwid.Text("No history to go back."))

    def handle_key(self, key):
        if key == 'enter':
            url = self.text_box.get_edit_text()
            self.navigate(url)
        elif key == 'b':
            self.go_back()
        elif key == 'q':
            raise urwid.ExitMainLoop()

if __name__ == "__main__":
    browser = TextBrowser()
    loop = urwid.MainLoop(browser.listbox, unhandled_input=browser.handle_key)

    try:
        loop.run()
    except KeyboardInterrupt:
        pass

