#!/usr/bin/env python3
"""
Title:       wavyt
Author:      Psyb3rN4ut
Date:        2018-09-16
Version:     0.01-r1
Python:      >=3.6
Description: "wavyt" is intended to be a pythonic command line tool for use 
              inside python interpereter such as "ipython" to make streaming 
              tunes without a GUI easy! "wavyt" makes use of "mpv" as 
              its player and scrapes using bs4.
"""
import requests
import subprocess
from bs4 import BeautifulSoup

dl_dir = "/home/psyb3rn4ut/Data/Downloads/wavyt/audio"
dl_ext = "/%(title)s.%(ext)s"
dl_format = "opus"
ext_player_cmd = ["mpv","--no-video"]
ext_dl_cmd = ["youtube-dl","-o","-x","--audio-format"]
base_url = "https://www.youtube.com"
search_url_ext = "/results?search_query="


class Search:
    """
    Scrapes base_url+search_url_ext, example: 
    "https://www.youtube.com/results?search_query=", 
    with search, example: "progressive trance".
    """
    def __init__(self, search):
        self.search = search
        self.query = "+".join(self.search.split())
        self.search_url = f"{base_url}{search_url_ext}{self.query}"
        self.resp = requests.get(self.search_url)
        self.cont = self.resp.content
        self.soup = BeautifulSoup(self.cont, 'html5lib')
        self.vid_links = self.soup.find_all('a', attrs={'class':'yt-uix-tile-link'})


class Track:
    """
    Object containing 'title' and 'url' of song along with 'play' and 'download' 
    methods to be called on self
    """
    
    def __init__(self, title, url, time):
        self.title = title
        self.url = url
        self.time = time
    
    def show(self):
        print(f"""
   Title   {self.title}
   URL     {self.url}
   Length  {self.time}
               """)

    def play(self):
        subprocess.Popen([ext_player_cmd[0],ext_player_cmd[1],self.url], 
        stdout=subprocess.DEVNULL, stderr=subprocess.STDOUT).wait()
    
    def download(self):
        subprocess.Popen([ext_dl_cmd[0],ext_dl_cmd[1],f"{dl_dir}{dl_ext}",
        ext_dl_cmd[2],ext_dl_cmd[3],dl_format, self.url]).wait()


class SearchList:
    """
    """
    def __ini__(self):
        self.searches = []

    def show(self, *args):
        """
        Loops over self.searches print()ing out double digit form of
        search position in self.searches and search.search.
        """
        if not args:
            for search in self.searches:
                print(f" {self.searches.index(search):0=2d} => {search.search}")
        else:
            for arg in args:
                self.searches[arg].show()
       

class TrackList:
    """
    """
    def __init__(self):
        self.tracks = []

    def show(self, *args):
        """
        Loops over self.tracks print()ing out double digit form of
        track position in self.tracks and track.title.
        """
        if not args:
            for track in self.tracks:
                print(
            f""" {self.tracks.index(track):0=2d} => [{track.time}] {track.title[0:76]}"""
                )
        else:
            for arg in args:
                self.tracks[arg].show()

    def play(self, *args):
        """
        Loops over self.tracks calling track.play() method belonging to "Track" 
        object. play() method accepts >=0 integer *args which are interperated 
        as track indeces in self.tracks
        """
        if not args:
            args = range(len(self.tracks))
        for arg in args:
            self.tracks[arg].show()
            self.tracks[arg].play()
     
    def download(self, *args):
        """
        Loops over self.tracks calling track.play() method from "Track" object.
        download() method accepts >=0 integer *args which are interperated as track
        indeces in self.tracks
        """
        if not args:
            args = range(len(self.tracks))
        for arg in args:
            self.tracks[arg].download()



class Player:
    """
    """
    def search(self, search):
        pass
        
    
    def shuffle(self):
        pass
 
