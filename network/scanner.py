## scanner.py

import socket
import sys
import threading
import queue

def is_port_open(host, port):
    try:
        sock=socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sock.connect((host, port))
    except socket.error:
       return(False)     
    return(True)


def scanner_worker_thread(host):
    while True:
        port=port_queue.get()
        if is_port_open(host, port):
            print("(%s) is OPEN!" % port)
        port_queue.task_done()


port_queue=queue.Queue()

for num in range(10):
    t=threading.Thread(target=scanner_worker_thread, kwargs={"host": sys.argv[1]})
    t.daemon=True
    t.start()


for port in range(40000,50000):
    port_queue.put(port)


port_queue.join()

